#!/bin/bash

odir=$1
filelist="http://data.csail.mit.edu/videogan/urls.txt"

while read -r line; do
  filename="$(basename "$line")"
  fullpath="$odir$filename"
  if [ ! -f "$fullpath.mjpeg" ]; then
    wget -q --show-progress --directory-prefix="$odir" "$line";
    bash downscale.sh "$fullpath" &
  fi
done < <(curl -s $filelist)