#!/bin/bash

fullpath=$1;

ffmpeg -loglevel quiet -i "$fullpath" -c:v mjpeg -vf scale=64:64 "$fullpath.mjpeg" < /dev/null;
rm "$fullpath" > /dev/null;