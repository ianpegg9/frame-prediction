"""
Takes AVI files and converts them to JPEG frames under the directory with the same name as the original file, but with
.mp4 appended instead of .avi
"""

from __future__ import print_function, division

import argparse
import os

import skvideo.io
import cv2


def video_gen(files):
    for fn in files:
        yield fn, skvideo.io.vread(fn)


def load_file_list(index_file):
    with open(index_file, 'r') as f:
        files = f.read().split('\n')[:-1]
    return files


if __name__ == '__main__':
    # tf.compat.v1.enable_eager_execution()

    parser = argparse.ArgumentParser()
    parser.add_argument('--index', type=str, help="index file with full path of every file to convert", required=True)
    parser.add_argument('--numfiles', type=int, help="Number of files to store the dataset in", default=10)
    args = parser.parse_args()

    files = load_file_list(args.index)
    videos = video_gen(files)

    for fn, video in videos:
        print(fn)
        odir = fn.replace('.avi', '.mp4')
        if not os.path.exists(odir):
            os.mkdir(odir)
        for i, frame in enumerate(video):
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            ofile = os.path.join(odir, '{:05d}.jpg'.format(i))
            cv2.imwrite(ofile, frame)
