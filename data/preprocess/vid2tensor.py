from __future__ import print_function, division
import argparse
import os

import skvideo.io
import tensorflow as tf


def _int64_feature(value):
    """Returns an int64_list from a bool / enum / int / uint."""
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _bytes_feature(value):
    """Returns a bytes_list from a string / byte."""
    if isinstance(value, type(tf.constant(0))):
        value = value.numpy()  # BytesList won't unpack a string from an EagerTensor.
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def serialize_example(class_name, video):
    """
    Return a serialized version of tf.train.Example

    Args:
        class_name: The class name string
        video: The video numpy array

    Returns:
        tf.string
    """
    print(class_name)
    feature = {
        'class': _bytes_feature(class_name),
        'video': _bytes_feature(tf.io.serialize_tensor(video)),
    }
    example_proto = tf.train.Example(features=tf.train.Features(feature=feature))
    return example_proto.SerializeToString()


def serialize_gen(files):
    for fn in files:
        class_name = os.path.dirname(os.path.dirname(fn)).split('/').pop()
        video = skvideo.io.vread(fn)
        yield serialize_example(class_name, video)


def tf_serialize_example(class_name, video):
    """
    serialize_example wrapped with tf.py_function
    See Also: serialize_example
    """
    tf_string = tf.py_function(
        serialize_example,
        (class_name, video),
        tf.string
    )
    return tf.reshape(tf_string, ())


def load_file_list(index_file):
    with open(index_file, 'r') as f:
        files = f.read().split('\n')[:-1]
    return files


def reduce_func(key, dataset):
    filename = tf.strings.join([DEST, tf.strings.as_string(key)])
    writer = tf.data.experimental.TFRecordWriter(filename)
    writer.write(dataset.map(lambda _, x: x))
    return tf.data.Dataset.from_tensors(filename)


if __name__ == '__main__':
    # tf.compat.v1.enable_eager_execution()

    parser = argparse.ArgumentParser()
    parser.add_argument('--index', type=str, help="index file with full path of every file to convert", required=True)
    parser.add_argument('--dest', type=str, help="Destination directory to store tfrecords", required=True)
    parser.add_argument('--numfiles', type=int, help="Number of files to store the dataset in", default=10)
    args = parser.parse_args()
    DEST = args.dest

    files = load_file_list(args.index)

    dataset = tf.data.Dataset.from_generator(
        serialize_gen, output_types=tf.string, output_shapes=(), args=(files,)
    )

    ofile = 'test.tfrecord'
    writer = tf.data.experimental.TFRecordWriter(ofile, compression_type='GZIP')
    writer.write(dataset)

    # This is supposed to write to multiple files, but does not
    # dataset = dataset.enumerate()
    # dataset = dataset.apply(tf.data.experimental.group_by_window(
    #     lambda i, _: i % args.numfiles, reduce_func, tf.int64.max
    # ))